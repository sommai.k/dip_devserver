# Recovery file

## copy local file to volume

```
docker run --rm  -v  dev-server_dev_data:/dev_data -v /home/dev:/home/source ubuntu:22.04 cp -R /home/source/training /dev_data
docker run --rm  -v  dev-server_dev_data:/dev_data -v /home/dev:/home/source ubuntu:22.04 cp -R /home/source/workspace /dev_data
```

# Backup file from volume

## copy file from volume to local

```
docker run --rm  -v  dev-server_dev_data:/dev_data -v /home/dev:/home/source ubuntu:22.04 cp -R /dev_data /home/source/training
docker run --rm  -v  dev-server_dev_data:/dev_data -v /home/dev:/home/source ubuntu:22.04 cp -R /dev_data /home/source/workspace

```

```
sudo vi /etc/default/jenkins

# port for HTTP connector (default 8080; disable with -1)
HTTP_PORT=8980


sudo vi /lib/systemd/system/jenkins.service

sudo systemctl daemon-reload

sudo systemctl restart jenkins
```

```
git clone https://gitlab.com/sommai.k/dip_devserver.git
```

```
docker-compose up -d node-20
docker compose up -d node-20
```

```
volumes:
  node_dev:
services:
  node-20:
    build:
      context: .
      dockerfile: docker/Dockerfile.node-20
    restart: on-failure
    command: [ "sleep", "infinity" ]
    volumes:
      - node_dev:/home/dev/source
```

## start server

```
docker compose up -d node-20
```

## stop server

```
docker compose stop node-20
```

## create new react project

```
npm create vite@latest project01 -- --template react-ts
```
